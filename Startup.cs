﻿using System;

namespace LPW_GuiaEjercicios1
{
    public class Startup
    {
        public static void Main(String[] args) {
            Ejercicio1();
        }

        #region ejercio1 
        public static void Ejercicio1(){
            Console.WriteLine("Calcular potencia");
            int x = 0, y = 0;
            do
            {
                Console.WriteLine("Ingrese el número base (entero)");
            } while (!int.TryParse(Console.ReadLine(), out x));
            do
            {
                Console.WriteLine("Ingrese el exponente (entero)");
            } while (!int.TryParse(Console.ReadLine(), out y));
        }

        public static int Potenciar(int x, int y) {
            double _base = x;
            double _exponente = y;
            return (int) Math.Pow(_base, _exponente);
        }
        #endregion
    }
}
